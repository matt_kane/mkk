﻿using Microsoft.Extensions.Configuration;

namespace MKK
{
    public static class ConfigurationSectionExtensions
    {
        public static bool GetBool(this IConfigurationSection cfg, string key)
        {
            bool.TryParse(cfg[key], out bool val);
            return val;
        }
    }
}