﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace MKK.NerveLib
{
    public class NerveNotification
    {
        public object Data { get; set; }

        public string Message { get; set; }
    }

    public class NerveClient : INerveClient
    {
        private readonly ConcurrentDictionary<Type, IDataflowBlock> _blks;

        public event EventHandler<NerveNotification> SendFailure;

        public INerveLogInterface Log { get; set; }

        public NerveClient(INerveLogInterface nerveLogInterface)
        {
            _blks = new ConcurrentDictionary<Type, IDataflowBlock>();
            Log = nerveLogInterface;
        }

        public void RegisterAsyncHandler<TData>(Func<TData, Task> doThis)
        {
            RegisterHandler<TData>(async t => await doThis(t));
        }

        public void RegisterHandler<TData>(Action<TData> doThis)
        {
            var type = typeof(TData);
            _blks.AddOrUpdate(
                typeof(TData),
                new ActionBlock<TData>(
                doThis,
                new ExecutionDataflowBlockOptions
                {
                    EnsureOrdered = true
                }),
                (t, b) => _blks[t] = b);
            
            Log?.Record(LogLevel.Info, $"Registered type '{type.FullName}'");
        }

        public async Task<bool> SendAsync<TData>(TData data)
        {
            var type = data.GetType();
            if (_blks.ContainsKey(type))
            {
                if (!(_blks[type] is ActionBlock<TData> blk))
                {
                    NotifySendFailure(
                        data,
                        $"Type '{type.FullName}' registered but no handler for type '{typeof(TData).FullName}' available");
                    return false;
                }

                return await blk.SendAsync(data).ConfigureAwait(false);
            }
            else
            {
                NotifySendFailure(
                    data,
                    $"Type '{type.FullName}' not registered");
                return false;
            }
        }

        private void NotifySendFailure<TData>(TData data, string message)
        {
            var notif =
                new NerveNotification { Data = data, Message = message };
            Log?.Record(LogLevel.Warn, message, data);
            SendFailure?.Invoke(this, notif);
        }
    }
}