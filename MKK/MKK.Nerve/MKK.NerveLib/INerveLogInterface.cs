﻿using System;

namespace MKK.NerveLib
{
    public enum LogLevel
    {
        Info,
        Debug,
        Warn,
        Error
    }

    public interface INerveLogInterface
    {
        public void Record(LogLevel logLevel, string message);

        public void Record(LogLevel logLevel, string message, params object[] data);

        public void Record(LogLevel logLevel, Exception exception, string message, params object[] data);
    }
}