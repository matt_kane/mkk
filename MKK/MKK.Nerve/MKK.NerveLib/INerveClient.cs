﻿using System;
using System.Threading.Tasks;

namespace MKK.NerveLib
{
    public interface INerveClient
    {
        INerveLogInterface Log { get; set; }

        void RegisterAsyncHandler<TMessageData>(Func<TMessageData, Task> doThis);

        void RegisterHandler<TMessageData>(Action<TMessageData> doThis);

        Task<bool> SendAsync<TMessageData>(TMessageData data);
    }
}