﻿using System;
using System.Runtime.Serialization;

namespace MKK.NerveLib
{
    [Serializable]
    public class NerveException : Exception
    {
        public NerveException()
        {
        }

        public NerveException(string message) : base(message)
        {
        }

        public NerveException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NerveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}