﻿using MKK;
using MKK.NerveLib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    public class App
    {
        private readonly INerveClient _nc;

        public App(INerveClient nc)
        {
            _nc = Guard.ParamNotNull(nc, nameof(nc));

            nc.RegisterHandler<GreetMsg>(msg =>
            {
                var p = new MsgProcessor();
                p.ProcessGreet(msg);
            });

            nc.RegisterHandler<StockMsg>(msg =>
            {
                var p = new MsgProcessor();
                p.ProcessStock(msg);
            });
            
        }

        public async Task StartAsync()
        {
            await _nc.SendAsync(
                new GreetMsg
                {
                    Name = "bob",
                    Greeting = "Yo!"
                });

            Console.WriteLine("Doing some stuff");
            for (int n=0; n<30; n++)
            {
                Console.WriteLine($"{n}...");
                if(n%7 == 0)
                {
                    await _nc.SendAsync(
                        new GreetMsg
                        {
                            Name = "bob",
                            Greeting = $"'n' is now {n}!"
                        });
                }

                if (n % 4 == 0)
                {
                    await _nc.SendAsync(
                        new StockMsg
                        {
                            StockTicker = "plop",
                            Price = n * 21
                        });
                }

                System.Threading.Thread.Sleep(1000);
            }

            await _nc.SendAsync(
                new GreetMsg
                {
                    Name = "bob",
                    Greeting = "Bye!"
                });
        }
    }
}
