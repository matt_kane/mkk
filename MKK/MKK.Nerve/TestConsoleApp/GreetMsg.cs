﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp
{
    public class GreetMsg
    {
        public string Name { get; set; }

        public string Greeting { get; set; }
    }
}
