﻿using MKK.NerveLib;
using SimpleInjector;
using System;
using System.Threading.Tasks;

namespace TestConsoleApp
{
    static class Program
    {
        private static Container _container = null;

        public static async Task Main(string[] args)
        {
            _container = new Container();
            CompositionRoot.Configure(_container);
            var app = _container.GetInstance<App>();
            await app.StartAsync();
        }
    }
}
