﻿using MKK.NerveLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp
{
    internal class MsgProcessor
    {
        public void ProcessGreet(GreetMsg msg)
        {
            Console.WriteLine(
                $"    {msg.Greeting} from {msg.Name}");
        }

        public void ProcessStock(StockMsg msg)
        {
            Console.WriteLine(
                $"    {msg.StockTicker} at price {msg.Price}");
        }
    }
}
