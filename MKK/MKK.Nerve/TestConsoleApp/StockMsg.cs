﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp
{
    public class StockMsg
    {
        public string StockTicker { get; set; }

        public decimal Price { get; set; }
    }
}
