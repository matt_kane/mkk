﻿using MKK.NerveLib;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestConsoleApp
{
    internal static class CompositionRoot
    {
        public static void Configure(SimpleInjector.Container ctr)
        {
            ctr.Register<INerveClient, NerveClient>();
        }
    }
}
