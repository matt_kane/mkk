﻿using Moq;
using SimpleInjector;
using System;

namespace MKK.NerveLib.Tests
{
    public static class AutoMockingContainer
    {
        public static Container Create()
        {
            var container = new Container();

            container.ResolveUnregisteredType += (s, e) =>
            {
                if (typeof(Mock).IsAssignableFrom(e.UnregisteredServiceType))
                {
                    e.Register(Lifestyle.Singleton.CreateRegistration(
                        e.UnregisteredServiceType,
                        () => Activator.CreateInstance(e.UnregisteredServiceType), container));
                }
                else if (e.UnregisteredServiceType.IsInterface)
                {
                    var mockType = typeof(Mock<>).MakeGenericType(e.UnregisteredServiceType);
                    e.Register(() => ((Mock)container.GetInstance(mockType)).Object);
                }
            };

            return container;
        }

        public static Mock<T> GetMockInstance<T>(this Container container) where T:class
        {
            return container.GetInstance<Mock<T>>();
        }
    }
}
