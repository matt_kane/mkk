﻿using System;

namespace MKK
{
    public static class Guard
    {
        public static T ParamNotNull<T>(T obj, string paramName)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(paramName);
            }

            return obj;
        }

        public static string NotEmptyOrWhitespace(string param, string paramName)
        {
            if (string.IsNullOrWhiteSpace(param))
            {
                throw new ArgumentException(paramName);
            }

            return param;
        }
    }
}