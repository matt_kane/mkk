﻿using MKK;
using MKK.NerveLib;
using Serilog;
using System;

namespace Demo
{

    public class SerilogNerveConnector: INerveLogInterface
    {
        private readonly ILogger _serilogger;

        public SerilogNerveConnector(ILogger serilogger)
        {
            _serilogger = Guard.ParamNotNull(serilogger, nameof(serilogger));
        }

        public void Record(LogLevel logLevel, string message) => LogFn(logLevel, null, message, null);

        public void Record(LogLevel logLevel, string message, params object[] data) => LogFn(logLevel, null, message, data);

        public void Record(LogLevel logLevel, Exception exception, string message, params object[] data) =>
            LogFn(logLevel, exception, message, data);

        private void LogFn(LogLevel logLevel, Exception exception, string message, params object[] data)
        {
            switch (logLevel)
            {
                case LogLevel.Debug:
                    _serilogger.Debug(exception, message, data);
                    break;

                case LogLevel.Error:
                    _serilogger.Error(exception, message, data);
                    break;

                case LogLevel.Warn:
                    _serilogger.Warning(exception, message, data);
                    break;

                default:
                    _serilogger.Information(exception, message, data);
                    break;
            }
        }
    }
}