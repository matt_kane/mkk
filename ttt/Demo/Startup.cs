﻿using Microsoft.Extensions.Configuration;

using MKK.NerveLib;
using Serilog;
using SimpleInjector;
using System;
using System.Threading.Tasks;
using tttLib.Ai;
namespace Demo
{
    internal static class Startup
    {
        public static async Task Main(string[] args)
        {
            if (args == null) { }

            var container = new Container();
            var builder = new ConfigurationBuilder()
                    .SetBasePath(System.AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json", true)
                    .AddJsonFile("appsettings.dev.json", true);

            var appSettings = new AppSettings(builder.Build());
            CompositionRoot(container, appSettings);
            var app = container.GetInstance<App>();
            await app.StartGamesAsync().ConfigureAwait(false);
        }

        private static void CompositionRoot(
            Container container,
            AppSettings appSettings)
        {
            container.RegisterInstance(appSettings);
            container.Register<IAi, SimpleAi>();
            container.Register<App>();
            RegisterLogging(container);

            container.RegisterSingleton<INerveClient, NerveClient>();
        }

        private static void RegisterLogging(
            Container container)
        {
            var logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                //.WriteTo.Console()
                .WriteTo.File("C:\\logs\\mkk\\ttt.txt", rollingInterval: RollingInterval.Hour)
                .CreateLogger();
            container.RegisterInstance<ILogger>(logger);

            container.RegisterSingleton<INerveLogInterface, SerilogNerveConnector>();
        }
    }
}