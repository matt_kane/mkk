﻿using Microsoft.Extensions.Configuration;

using MKK;

namespace Demo
{
    internal class AppSettings
    {
        public string Version { get; set; }

        public AppSettings(IConfigurationRoot config)
        {
            Guard.ParamNotNull(config, nameof(config));

            //load values in - this needs to be done after the defaults are set
            var cfg = config.GetSection("config");

            SetVersions();
        }

        private void SetVersions()
        {
            if (!string.IsNullOrEmpty(Version))
                return;

            var attr = System.Reflection.Assembly.GetExecutingAssembly().GetName();
            Version = attr.Version.ToString();
        }
    }
}