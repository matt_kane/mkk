﻿using MKK;
using MKK.NerveLib;

using System;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

using tttLib;
using tttLib.Ai;

namespace Demo
{
    public class App
    {
        private readonly IAi _ai;
        private readonly INerveClient _nerveClient;
        private bool isAiThinking = false;

        public App(IAi ai, INerveClient nerveClient)
        {
            _ai = Guard.ParamNotNull(ai, nameof(ai));
            _nerveClient = Guard.ParamNotNull(nerveClient, nameof(nerveClient));

            _nerveClient.RegisterAsyncHandler<HumanMoveRequest>(HumanMoveRequested);
            _nerveClient.RegisterAsyncHandler<AiPlayerMoveRequest>(AiMoveRequested);
            isAiThinking = false;
        }

        private async Task HumanMoveRequested(HumanMoveRequest humanMoveRequest)
        {
            if (humanMoveRequest?.Cell.HasValue ?? false)
            {
                humanMoveRequest.Game.Place(humanMoveRequest.Cell.Value);
                DisplayBoard(humanMoveRequest.Game);
                Console.Write("\n\nThinking ");
                var aiMoveRequest = await _ai.ComputeBestNextMoveAsync(humanMoveRequest.Game);
                await SendNerveMessageAsync(aiMoveRequest);
            }
        }

        private async Task AiMoveRequested(AiPlayerMoveRequest aiMoveRequest)
        {
            if (aiMoveRequest?.Cell.HasValue ?? false)
            {
                aiMoveRequest.Game.Place(aiMoveRequest.Cell.Value);
                DisplayBoard(aiMoveRequest.Game);
                var humanMoveRequest = await GetUserMoveAsync(aiMoveRequest.Game);
                await SendNerveMessageAsync(humanMoveRequest);
            }
        }

        private void Message(string msg)
        {
            Console.WriteLine(msg);
        }

        public async Task StartGamesAsync()
        {
            var quit = false;
            while (!quit)
            {
                await PlayGameAsync();
                quit = !GetUserYesNo("Another game");
            }
        }

        private async Task PlayGameAsync()
        {
            var game = new Game();
            game.GameWon += (player) =>
            {
                DisplayBoard(game);
                Message($"{player} wins the game");
            };

            game.GameDrawn += () => Message($"This game is a draw");
            DisplayBoard(game);
            var humanMoveRequest = await GetUserMoveAsync(game);
            var t = HumanMoveRequested(humanMoveRequest);
            while (!game.IsGameOver)
            {
                //await Task.CompletedTask;
                //if(!isThinking)
                //{

                //}
                // t.Wait();
            }
        }

        private async Task SendNerveMessageAsync<TData>(TData data)
        {
            Message($"_nerveClient.SendAsync({data})");
            await _nerveClient.SendAsync(data);
        }

        private void DisplayBoard(Game game)
        {
            Console.Clear();
            Message($"\n{game}");
        }

        private async Task<HumanMoveRequest> GetUserMoveAsync(Game game)
        {
            string strIn = string.Empty;
            DateTime dt = DateTime.Now;
            TimeSpan ts =new TimeSpan();
            while (strIn.Length == 0)
            {
                Console.Write("\n\nInput move (row[1-3], col[A-C] or ZZ for AI, or QUIT: ");
                strIn = Console.ReadLine().Trim().ToUpperInvariant();
                ts = DateTime.Now - dt;
            }

            if (strIn.StartsWith("Q"))
            {
                game.IsGameOver = true;
                return null;
            }
            if(strIn.StartsWith("Z"))
            {
                await SendNerveMessageAsync(game);
                return null;
            }

            var parsedOk = Cell.TryParse(strIn, out Cell cell);
            if(parsedOk)
            {
                return new HumanMoveRequest(game, cell, ts);
            }
            else
            {
                return null;
            }
        }

        private bool GetUserYesNo(string message)
        {
            bool? q = null;
            Console.Write($"{message ?? string.Empty} (Y/N) ? ");

            while (!q.HasValue)
            {
                switch (char.ToUpperInvariant(Console.ReadKey(false).KeyChar))
                {
                    case 'Y':
                        q = true;
                        break;

                    case 'N':
                        q = false;
                        break;

                    default:
                        break;
                }
            }

            return q.Value;
        }
    }
}