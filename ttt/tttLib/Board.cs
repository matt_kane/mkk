﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace tttLib
{
    public class Board
    {
        public BitBoard Occupied => X.Union(O);
        public BitBoard Unoccupied => Occupied.Invert();

        public BitBoard X { get; private set; }
        public BitBoard O { get; private set; }

        public Board(BitBoard o, BitBoard x)
        {
            if (o.Intersect(x) != BitBoard.Empty)
            {
                throw new InvalidOperationException(
                    "No x and o can occupy the same position on the board");
            }

            O = o;
            X = x;
        }

        public Board() : this(BitBoard.Empty, BitBoard.Empty)
        {
        }

        public bool IsBoardFull => Occupied == BitBoard.Full;

        private byte[][] ToByteArrays()
        {
            var arr = new byte[3][];

            uint mask = 1;
            for (int r = 2; r >= 0; r--)
            {
                arr[r] = new byte[3];
                for (int c = 2; c >= 0; c--)
                {
                    var n = ((O._data & mask) == 0 ? 0 : 1) | (((X._data & mask) == 0 ? 0 : 1) << 1);
                    arr[r][c] = (byte)(n & 0x00ff);
                    mask <<= 1;
                }
            }

            return arr;
        }

        public override string ToString()
        {
            var ca = ToByteArrays()
                .Select(
                    row => row.Select(
                        c => c switch
                                {
                                    2 => 'X',
                                    1 => 'O',
                                    _ => ' ',
                                }).ToArray())
                .ToArray();

            var template =
                $"{ca[0][0]}|{ca[0][1]}|{ca[0][2]}\n" +
                "-----\n" +
                $"{ca[1][0]}|{ca[1][1]}|{ca[1][2]}\n" +
                "-----\n" +
                $"{ca[2][0]}|{ca[2][1]}|{ca[2][2]}\n";
            return template;
        }

        public Board Place(Cell move, Player player)
        {
            var bb = move.ToBitBoard();
            if (player == Player.Noughts)
            {
                return Drop(bb, () => new Board(O.Union(bb), X));
            }
            else
            {
                return Drop(bb, () => new Board(O, X.Union(bb)));
            }
        }

        private Board Drop(BitBoard places, Func<Board> brdFn)
        {
            if (Occupied.Intersect(places) != BitBoard.Empty)
            {
                throw new InvalidOperationException(
                    "Board position is already occupied");
            }

            return brdFn();
        }

        public bool Is_X_Win
        {
            get
            {
                return WinningStates.Any(bb => bb.Intersect(X) == bb);
            }
        }

        public bool Is_O_Win
        {
            get
            {
                return WinningStates.Any(bb => bb.Intersect(O) == bb);
            }
        }

        public static IEnumerable<BitBoard> WinningStates
        {
            get
            {
                yield return StandardBitBoards.Top;
                yield return StandardBitBoards.MiddleRow;
                yield return StandardBitBoards.Bottom;
                yield return StandardBitBoards.Left;
                yield return StandardBitBoards.MiddleCol;
                yield return StandardBitBoards.Right;
                yield return StandardBitBoards.Diagonal1;
                yield return StandardBitBoards.Diagonal2;
            }
        }
    }
}