﻿using System;

namespace tttLib
{
    public class HumanMoveRequest : MoveRequest
    {
        public HumanMoveRequest(Game game, Cell cell, TimeSpan thinkingTime)
            : base(game, cell)
        {
            ThinkingTime = thinkingTime;
        }
    }
}