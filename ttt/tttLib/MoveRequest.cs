﻿using MKK;
using System;

namespace tttLib
{
    public abstract class MoveRequest
    {
        public MoveRequest(Game game, Cell cell)
        {
            Game = Guard.ParamNotNull(game, nameof(game));
            Cell = Guard.ParamNotNull(cell, nameof(cell));
        }

        public Game Game { get; private set; }

        public Cell? Cell { get; private set; }

        public TimeSpan ThinkingTime { get; set; }
    }
}