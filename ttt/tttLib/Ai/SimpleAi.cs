﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tttLib.Ai
{
    public class SimpleAi : IAi
    {
        private List<Strategy> Strategies;

        public SimpleAi()
        {
            Strategies = new List<Strategy>();

            AddStrategy(
                "acquire centre board if available",
                game => game.Board.Unoccupied.Intersect(StandardBitBoards.Centre),
                3,
                true);

            AddStrategy(
                "acquire corners if available",
                game => game.Board.Unoccupied.Intersect(StandardBitBoards.Corners),
                2,
                true);

            AddStrategy("acquire centre edges if available",
                game => game.Board.Unoccupied.Intersect(StandardBitBoards.Middle),
                1,
                true);

            AddStrategy("Always select winning move if available",
                game => SelectWinningMoves(game.Board, game.TurnToPlay),
                int.MaxValue,
                false);

            AddStrategy("Always block opponent winning move if available",
                game => SelectWinningMoves(game.Board, game.TurnToPlay.Flip()),
                1000,
                false);
        }

        private BitBoard SelectWinningMoves(Board board, Player player)
        {
            var playerOccupiedBitBoard =
                player == Player.Noughts ? board.O : board.X;
            var availableMoves = board.Occupied.GetUnmarkedCellBitBoards();
            var winningMoves = availableMoves.Where(
                bb => Board.WinningStates.Contains(
                    bb.Union(playerOccupiedBitBoard)));
            return winningMoves.FirstOrDefault();
        }

        private void AddStrategy(
            string description,
            Func<Game, BitBoard> viableStrategyFn,
            int addScoreOnMatch,
            bool isStrategyRemovableWhenNoLongerViable)
        {
            var newStrategy = new Strategy
            {
                Description = description,
                ViableStrategyFn = viableStrategyFn,
                AddScoreOnMatch = addScoreOnMatch,
                IsStrategyRemovableWhenNoLongerViable =
                    isStrategyRemovableWhenNoLongerViable
            };

            Strategies.Add(newStrategy);
        }

        private void AppendScoredMoves(
            List<AiPlayerMoveRequest> scoredMoves,
            IEnumerable<AiPlayerMoveRequest> movesToAdd)
        {
            scoredMoves.AddRange(movesToAdd);
        }

        private AiPlayerMoveRequest MaxScoredMove(IEnumerable<AiPlayerMoveRequest> scoredMoves)
        {
            var max = scoredMoves.Max(m => m.Score);
            return scoredMoves.FirstOrDefault(m => m.Score == max);
        }

        private IEnumerable<AiPlayerMoveRequest> ScoreMoves(
            Game game,
            Strategy strategy,
            BitBoard possibleMoveBitBoard)
        {
            return possibleMoveBitBoard.ToCells().Select(cell =>
               new AiPlayerMoveRequest(game, cell, strategy)
               {
                   Score = strategy.AddScoreOnMatch
               });
        }

        public Task<AiPlayerMoveRequest> ComputeBestNextMoveAsync(Game game)
        {
            return Task.Run(() =>
            {
                var dt = DateTime.Now;
                var removableStrategies = new List<Strategy>();
                var scoredMoveList = new List<AiPlayerMoveRequest>();

                foreach (var strategy in Strategies)
                {
                    var viableMoveBitboard = strategy.ViableStrategyFn(game);
                    var isViableMoveAvailableWithThisStrategy = viableMoveBitboard != StandardBitBoards.Empty;

                    if (isViableMoveAvailableWithThisStrategy)
                    {
                        var strategyScoredMoves = ScoreMoves(game, strategy, viableMoveBitboard);
                        AppendScoredMoves(scoredMoveList, strategyScoredMoves);
                    }
                    else
                    {
                        if (strategy.IsStrategyRemovableWhenNoLongerViable)
                        {
                            removableStrategies.Add(strategy);
                        }
                    }
                }

                foreach (var strategy in removableStrategies)
                {
                    Strategies.Remove(strategy);
                }

                var move = MaxScoredMove(scoredMoveList);
                move.ThinkingTime = DateTime.Now - dt;
                System.Threading.Thread.Sleep(3333);
                return move;
            });
        }
    }
}