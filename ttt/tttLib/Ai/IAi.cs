﻿using System.Threading.Tasks;

namespace tttLib.Ai
{
    public interface IAi
    {
        Task<AiPlayerMoveRequest> ComputeBestNextMoveAsync(Game game);
    }
}