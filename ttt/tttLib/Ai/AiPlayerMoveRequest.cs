﻿using MKK;

namespace tttLib.Ai
{
    public class AiPlayerMoveRequest : MoveRequest
    {
        public AiPlayerMoveRequest(Game game, Cell cell, Strategy strategy)
            : base(game, cell)
        {
            Strategy = Guard.ParamNotNull(strategy, nameof(strategy));
        }

        public Strategy Strategy { get; set; }

        public int Hashcode => Cell.GetHashCode();

        public int Score { get; set; }
    }
}