﻿using System;

namespace tttLib.Ai
{
    public class Strategy
    {
        public string Description { get; set; }

        internal Func<Game, BitBoard> ViableStrategyFn { get; set; }

        internal bool IsStrategyRemovableWhenNoLongerViable { get; set; }

        public int AddScoreOnMatch { get; set; }
    }
}