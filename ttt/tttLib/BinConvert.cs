﻿using System;
using System.Linq;

namespace tttLib
{
    using BinType = UInt32;

    internal static class BinConvert
    {
        public static BinType BinStr2Bin(string binStr, int requiredCharCount)
        {
            var pattern = new string(binStr.Where(c => c == '0' || c == '1').ToArray());

            if (pattern.Length != requiredCharCount)
            {
                throw new ArgumentException($"Invalid {requiredCharCount}-character binary string: '{binStr.Trim()}'");
            }

            BinType data = 0;
            for (int count = 0; count < requiredCharCount; count++)
            {
                data = data << 1;
                if (pattern[count] == '1') data++;
            }

            return data;
        }
    }
}