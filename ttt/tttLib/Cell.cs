﻿using MKK;

using System;
using System.Linq;

namespace tttLib
{
    public struct Cell : IEquatable<Cell>
    {
        private readonly char _row;
        private readonly char _col;

        private const string ValidRowChars = "123";
        private const string ValidColChars = "ABC";

        private static readonly string ValidChars =
            string.Concat(ValidRowChars, ValidColChars);

        public Cell(char col, int row)
        {
            var nCol = char.ToUpperInvariant(col);
            var nRow = char.ToUpperInvariant((char)(row & 0xff));
            if (nCol >= 'A' && nCol <= 'C')
            {
                _col = nCol;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(col));
            }

            if (nRow >= 1 && nRow <= 3)
            {
                _row = nRow;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(row));
            }
        }

        public static Cell Parse(string val)
        {
            var strippedInput = new string(
                Guard.NotEmptyOrWhitespace(val, nameof(val))
                .ToUpperInvariant()
                .Where(ValidChars.Contains)
                .ToArray());

            if (strippedInput.Length != 2)
            {
                throw new ArgumentException("Invalid cell reference", nameof(val));
            }

            var col = strippedInput.FirstOrDefault(ValidColChars.Contains);
            var row = strippedInput.FirstOrDefault(ValidRowChars.Contains);
            if (col == 0 || row == 0)
            {
                throw new ArgumentException("Invalid cell reference", nameof(val));
            }

            return new Cell(col, row - '1' + 1);
        }

        public static bool TryParse(string val, out Cell cell)
        {
            try
            {
                cell = Cell.Parse(val);
                return true;
            }
            catch (ArgumentException aEx)
            {
                if (aEx.Message.StartsWith("Invalid cell reference"))
                {
                    cell = default;
                    return false;
                }

                throw;
            }
        }

        internal BitBoard ToBitBoard()
        {
            var r = _row - 1;
            var c = 3 - _col;
            var n = (r * 3) + c;

            return BitBoard.Empty.SetBit(n);
        }

        public static explicit operator Cell(string operand)
        {
            return Cell.Parse(operand);
        }

        public override bool Equals(object obj)
        {
            var other = (Cell)obj;
            return Equals(other);
        }

        public override int GetHashCode() => HashCode.Combine(_row, _col);

        public static bool operator ==(Cell left, Cell right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Cell left, Cell right)
        {
            return !(left == right);
        }

        public bool Equals(Cell other)
        {
            return _row == other._row && _col == other._col;
        }

        public override string ToString()
        {
            return $"{_col}{(int)_row}";
        }
    }
}