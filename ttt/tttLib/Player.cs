﻿namespace tttLib
{
    public enum Player
    {
        Noughts,
        Crosses
    }

    public static class PlayerExtensions
    {
        public static Player Flip(this Player player)
        {
            return (player == Player.Noughts)
                ? Player.Crosses
                : Player.Noughts;
        }
    }
}