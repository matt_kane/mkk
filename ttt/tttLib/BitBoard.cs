﻿using System;
using System.Collections.Generic;

namespace tttLib
{
    using BinType = UInt32;

    public struct BitBoard : IEquatable<BitBoard>
    {
        internal readonly BinType _data;
        private const int CellCount = 3 * 3;

        public BitBoard(string pattern)
        {
            _data = BinConvert.BinStr2Bin(pattern, CellCount);
        }

        public static bool operator ==(BitBoard lhs, BitBoard rhs)
        {
            return lhs._data.Equals(rhs._data);
        }

        public static bool operator !=(BitBoard lhs, BitBoard rhs)
        {
            return !lhs._data.Equals(rhs._data);
        }

        public BitBoard(BinType data)
        {
            _data = data;
        }

        public BitBoard Intersect(BitBoard bitBoard)
        {
            return new BitBoard(_data & bitBoard._data);
        }

        public BitBoard NotIntersectingWith(BitBoard bitBoard)
        {
            return Intersect(bitBoard.Invert());
        }

        public BitBoard Union(BitBoard bitBoard)
        {
            return new BitBoard(_data | bitBoard._data);
        }

        public BitBoard Invert()
        {
            return new BitBoard(_data ^ 0x1ff);
        }

        public int GetSetBitsCount()
        {
            // Brian Kernighan’s algorithm to count the number of bits set
            // in a uint
            var n = _data & 511; // mask off unused most significant bits

            int count = 0;
            while (n > 0)
            {
                n &= (n - 1);
                count++;
            }

            return count;
        }

        public IEnumerable<BitBoard> GetUnmarkedCellBitBoards()
        {
            var n = 1;
            BinType bit = 1;

            while (n <= CellCount)
            {
                if ((_data & bit) == 0)
                {
                    yield return new BitBoard(bit);
                }

                n++;
                bit <<= 1;
            }
        }

        public override string ToString()
        {
            var data = _data;
            var str = "";
            int r = 1;
            for (int count = 0; count < CellCount; count++)
            {
                if ((data & 256) == 256) str += "1"; else str += "0";
                if (r == 3) { str += "\n"; r = 0; }
                data <<= 1;
                r++;
            }

            return str.Trim();
        }

        public override bool Equals(object obj)
        {
            var other = (BitBoard)obj;
            return other.Equals(this);
        }

        public bool Equals(BitBoard other)
        {
            return _data == other._data;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_data);
        }

        public BitBoard SetBit(int n)
        {
            return new BitBoard(_data | 1U << n);
        }

        public IEnumerable<Cell> ToCells()
        {
            if ((_data & 0x0004) != 0)
                yield return new Cell('A', 1);
            if ((_data & 0x0002) != 0)
                yield return new Cell('B', 1);
            if ((_data & 0x0001) != 0)
                yield return new Cell('C', 1);
            if ((_data & 0x0020) != 0)
                yield return new Cell('A', 2);
            if ((_data & 0x0010) != 0)
                yield return new Cell('B', 2);
            if ((_data & 0x0008) != 0)
                yield return new Cell('C', 2);
            if ((_data & 0x0100) != 0)
                yield return new Cell('A', 3);
            if ((_data & 0x0080) != 0)
                yield return new Cell('B', 3);
            if ((_data & 0x0040) != 0)
                yield return new Cell('C', 3);
        }

        public static readonly BitBoard Empty = new BitBoard(
              "000"
            + "000"
            + "000");

        public static readonly BitBoard Full = new BitBoard(
              "111"
            + "111"
            + "111");
    }
}