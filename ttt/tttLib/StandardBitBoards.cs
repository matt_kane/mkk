﻿namespace tttLib
{
    internal static class StandardBitBoards
    {
        public static readonly BitBoard Empty = new BitBoard(
              "000"
            + "000"
            + "000");

        public static readonly BitBoard Bottom = new BitBoard(
              "000"
            + "000"
            + "111");

        public static readonly BitBoard Left = new BitBoard(
              "100"
            + "100"
            + "100");

        public static readonly BitBoard MiddleCol = new BitBoard(
              "010"
            + "010"
            + "010");

        public static readonly BitBoard MiddleRow = new BitBoard(
              "000"
            + "111"
            + "000");

        public static readonly BitBoard Right = new BitBoard(
              "001"
            + "001"
            + "001");

        public static readonly BitBoard Top = new BitBoard(
              "111"
            + "000"
            + "000");

        public static readonly BitBoard Diagonal1 = new BitBoard(
              "001"
            + "010"
            + "100");

        public static readonly BitBoard Diagonal2 = new BitBoard(
              "100"
            + "010"
            + "001");

        public static readonly BitBoard Middle;
        public static readonly BitBoard Centre;
        public static readonly BitBoard Edge;
        public static readonly BitBoard Corners;

        static StandardBitBoards()
        {
            Middle = MiddleRow.Union(MiddleCol);
            Centre = MiddleRow.Intersect(MiddleCol);
            Edge = Top.Union(Bottom).Union(Left).Union(Right);
            Corners = Middle.Invert();
        }
    }
}