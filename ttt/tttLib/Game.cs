﻿using System;
using System.Linq;

namespace tttLib
{
    public class Game
    {
        public Board Board { get; set; }

        public bool IsGameOver { get; set; }

        public Player TurnToPlay { get; set; }

        public event Action<Player> GameWon;

        public event Action GameDrawn;

        public int MoveCount { get; set; }

        public Game()
        {
            TurnToPlay = Player.Noughts;
            IsGameOver = false;
            Board = new Board();
        }

        public override string ToString()
        {
            if (!IsGameOver)
            {
                return $"{DisplayBoard(Board)}\n" +
                    $"{Board.Unoccupied.ToCells().Select(c => c.ToString()).Aggregate((s1, s2) => $"{s1}, {s2}")}\n" +
                    $"{TurnToPlay} to play\n";
            }
            else
            {
                return $"{DisplayBoard(Board)}\nGame Over\n";
            }
        }

        private string DisplayBoard(Board brd)
        {
            var strOut = "  A B C\n";
            var brdStr = brd.ToString();
            var sa = brdStr.Split('\n');
            var count = 1;
            foreach (var str in sa)
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    strOut += "\n";
                }
                else if (str.StartsWith("--", StringComparison.InvariantCulture))
                {
                    strOut += $"  {str}\n";
                }
                else
                {
                    strOut += $"{4 - count++} {str}\n";
                }
            }

            return strOut;
        }

        public void Place(Cell move)
        {
            Board = Board.Place(move, TurnToPlay);
            if (!IsGameOver) IsGameOver = ComputeIsGameOver();
            TurnToPlay = TurnToPlay.Flip();
            MoveCount++;
        }

        private bool ComputeIsGameOver()
        {
            if (Board.Is_O_Win)
            {
                GameWon?.Invoke(Player.Noughts);
                return true;
            }

            if (Board.Is_X_Win)
            {
                GameWon?.Invoke(Player.Crosses);
                return true;
            }

            if (Board.IsBoardFull)
            {
                GameDrawn?.Invoke();
                return true;
            }

            return false;
        }
    }
}