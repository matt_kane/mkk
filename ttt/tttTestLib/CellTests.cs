﻿using Shouldly;

using System;
using System.Collections.Generic;
using System.Linq;

using tttLib;

using Xunit;

namespace tttTestLib
{
    public class CellTests
    {
        public CellTests()
        {
        }

        [Fact]
        public void Parse_DifferentFormats_Test()
        {
            Cell.Parse("b2").ToBitBoard().MustBe("000 010 000");
            Cell.Parse("(b, 2)").ToBitBoard().MustBe("000 010 000");
            Cell.Parse("b-2").ToBitBoard().MustBe("000 010 000");
            Cell.Parse("2b").ToBitBoard().MustBe("000 010 000");
            Cell.Parse("2, ,,4mgems,[-!(b]").ToBitBoard().MustBe("000 010 000");
        }

        [Fact]
        public void ParseSuccessTest()
        {
            Should.NotThrow(() => Cell.Parse("b2"));

            Cell.Parse("a1").ToBitBoard().MustBe("000 000 100");
            Cell.Parse("a2").ToBitBoard().MustBe("000 100 000");
            Cell.Parse("a3").ToBitBoard().MustBe("100 000 000");

            Cell.Parse("b1").ToBitBoard().MustBe("000 000 010");
            Cell.Parse("b2").ToBitBoard().MustBe("000 010 000");
            Cell.Parse("b3").ToBitBoard().MustBe("010 000 000");

            Cell.Parse("c1").ToBitBoard().MustBe("000 000 001");
            Cell.Parse("c2").ToBitBoard().MustBe("000 001 000");
            Cell.Parse("c3").ToBitBoard().MustBe("001 000 000");
        }

        [Fact]
        public void ParseFailTest()
        {
            Should.Throw<ArgumentException>(() => Cell.Parse(null));
            Should.Throw<ArgumentException>(() => Cell.Parse(""));
            Should.Throw<ArgumentException>(() => Cell.Parse("d4"));
            Should.Throw<ArgumentException>(() => Cell.Parse("a4"));
            Should.Throw<ArgumentException>(() => Cell.Parse("c4"));
            Should.Throw<ArgumentException>(() => Cell.Parse("4c"));
            Should.Throw<ArgumentException>(() => Cell.Parse("d1"));
            Should.Throw<ArgumentException>(() => Cell.Parse("d3"));
            Should.Throw<ArgumentException>(() => Cell.Parse("0"));
            Should.Throw<ArgumentException>(() => Cell.Parse("test"));

            Should.Throw<ArgumentException>(() => Cell.Parse("aa"));
            Should.Throw<ArgumentException>(() => Cell.Parse("11"));
            Should.Throw<ArgumentException>(() => Cell.Parse("bb"));
            Should.Throw<ArgumentException>(() => Cell.Parse("22"));
            Should.Throw<ArgumentException>(() => Cell.Parse("cc"));
            Should.Throw<ArgumentException>(() => Cell.Parse("33"));
        }

        [Fact]
        public void EqualsTest()
        {
            Cell.Parse("a1").Equals(Cell.Parse("a1")).ShouldBeTrue();
            Cell.Parse("a1").Equals(Cell.Parse("1a")).ShouldBeTrue();
            Cell.Parse("b2").Equals(Cell.Parse("2b")).ShouldBeTrue();
            Cell.Parse("c3").Equals(Cell.Parse("3c")).ShouldBeTrue();

            Cell.Parse("c3").Equals((Cell)"3c").ShouldBeTrue();

            Should.Throw<InvalidCastException>(() => Cell.Parse("a1").Equals(32));

            object obj = new Cell('b', 2);
            Cell m = Cell.Parse("B2");
            m.Equals(obj).ShouldBeTrue();
            obj.Equals(m).ShouldBeTrue();
        }

        [Fact]
        public void GetHashCodeTest()
        {
            var hashList = new List<(int row, char col, int hash)>();
            for (var row = 1; row <= 3; row++)
            {
                for (var col = 'A'; col <= 'C'; col++)
                {
                    var move = new Cell(col, row);
                    var hash = move.GetHashCode();
                    hashList.Add((row, col, hash));
                }
            }

            hashList.Count.ShouldBe(9);
            hashList.Distinct().Count().ShouldBe(9);
        }

        [Fact]
        public void EqTest()
        {
            (Cell.Parse("b2") == (Cell)"2b").ShouldBeTrue();
            (Cell.Parse("b2") == (Cell)"2c").ShouldBeFalse();
        }

        [Fact]
        public void NeqTest()
        {
            (Cell.Parse("b2") != (Cell)"2b").ShouldBeFalse();
            (Cell.Parse("b2") != (Cell)"2c").ShouldBeTrue();
        }

        [Fact]
        public void ToStringTest()
        {
            var m = new Cell('b', 2);
            m.ToString().ShouldBe("B2");
        }
    }
}