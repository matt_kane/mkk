using Shouldly;

using System;

using tttLib;

using Xunit;

namespace tttTestLib
{
    public class BoardTests
    {
        //private readonly IFixture fixture;

        public BoardTests()
        {
            //// Fixture setup
            //fixture = new Fixture()
            //    .Customize(new AutoMoqCustomization());
        }

        //[Fact]
        //public void BoardTest()
        //{
        //    var boardMock = fixture.Create<IBoard>();
        //}

        [Fact]
        public void CtorTests()
        {
            // X and O on centre square should throw
            Should.Throw<InvalidOperationException>(() =>
                new Board(
                    StandardBitBoards.Middle,
                    StandardBitBoards.Diagonal1.Union(
                        StandardBitBoards.Diagonal2)));
        }

        [Fact]
        public void ToString_EmptyBoardTest()
        {
            new Board().ToString().ShouldBe(
                " | | \n" +
                "-----\n" +
                " | | \n" +
                "-----\n" +
                " | | \n");
        }

        [Fact]
        public void ToString_PatternTest()
        {
            new Board(StandardBitBoards.Middle, StandardBitBoards.Corners)
                .ToString()
                .ShouldBe(
                    "X|O|X\n" +
                    "-----\n" +
                    "O|O|O\n" +
                    "-----\n" +
                    "X|O|X\n");
        }

        [Fact]
        public void OccupiedTest()
        {
            var b = new Board(StandardBitBoards.Top, StandardBitBoards.Bottom);
            b.Occupied.MustBe(
                "111" +
                "000" +
                "111");
        }

        [Fact]
        public void UnccupiedTest()
        {
            var b = new Board(StandardBitBoards.Top, StandardBitBoards.Bottom);
            b.Unoccupied.MustBe(
                "000" +
                "111" +
                "000");
        }

        [Fact]
        public void PlaceTest()
        {
            var b = new Board();

            b.Place((Cell)"A3", Player.Noughts)
                .ToString()
                .ShouldBe(
                "O| | \n" +
                "-----\n" +
                " | | \n" +
                "-----\n" +
                " | | \n");

            b.Place((Cell)"C1", Player.Crosses)
                .ToString()
                .ShouldBe(
                " | | \n" +
                "-----\n" +
                " | | \n" +
                "-----\n" +
                " | |X\n");
        }
    }
}