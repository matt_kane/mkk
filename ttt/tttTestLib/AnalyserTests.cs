﻿using Shouldly;

using System.Collections.Generic;

using tttLib;

using Xunit;

namespace tttTestLib
{
    public class AnalyserTests
    {
        [Theory]
        [MemberData(nameof(WinningStates))]
        internal void X_Win_Tests(BitBoard xstate, bool isWinning)
        {
            var b = new Board(BitBoard.Empty, xstate);
            b.Is_O_Win.ShouldBe(false);
            b.Is_X_Win.ShouldBe(isWinning);
        }

        [Theory]
        [MemberData(nameof(WinningStates))]
        internal void O_Win_Tests(BitBoard ostate, bool isWinning)
        {
            var b = new Board(ostate, BitBoard.Empty);
            b.Is_O_Win.ShouldBe(isWinning);
            b.Is_X_Win.ShouldBe(false);
        }

        public static IEnumerable<object[]> WinningStates()
        {
            yield return new object[] { StandardBitBoards.Top, true };
            yield return new object[] { StandardBitBoards.MiddleRow, true };
            yield return new object[] { StandardBitBoards.Bottom, true };
            yield return new object[] { StandardBitBoards.Left, true };
            yield return new object[] { StandardBitBoards.MiddleCol, true };
            yield return new object[] { StandardBitBoards.Right, true };
            yield return new object[] { StandardBitBoards.Diagonal1, true };
            yield return new object[] { StandardBitBoards.Diagonal2, true };
        }
    }
}