using Shouldly;

using tttLib;

using Xunit;

namespace tttTestLib
{
    public class StandardBitBoardTests
    {
        [Fact]
        public void StandardBitBoardsTest()
        {
            BitBoard.Empty.MustBe("000" +
                "000" +
                "000");

            StandardBitBoards.Top.MustBe("111" +
                "000" +
                "000");

            StandardBitBoards.Middle.MustBe("010" +
                "111" +
                "010");

            StandardBitBoards.Bottom.MustBe("000" +
                "000" +
                "111");

            StandardBitBoards.Left.MustBe("100" +
                "100" +
                "100");

            StandardBitBoards.Right.MustBe("001" +
                "001" +
                "001");

            StandardBitBoards.MiddleRow.Intersect(StandardBitBoards.MiddleCol)
                .MustBe(
                "000" +
                "010" +
                "000");

            StandardBitBoards.MiddleCol.Intersect(StandardBitBoards.MiddleRow)
                .MustBe(
                "000" +
                "010" +
                "000");
        }

        [Fact]
        public void EqualityTests()
        {
            StandardBitBoards.Corners
                .ShouldBe(new BitBoard(
                    "101" +
                    "000" +
                    "101"));
            var bb1 = StandardBitBoards.Corners;
            var bb2 = new BitBoard(
                    "101" +
                    "000" +
                    "101");
            bb1.ShouldBe(bb2);
            bb1.Equals(bb2).ShouldBeTrue();
            (bb1 == bb2).ShouldBeTrue();
        }

        [Fact]
        public void Invert()
        {
            StandardBitBoards.Middle.Invert()
                .ShouldBe(StandardBitBoards.Corners);
        }

        [Fact]
        public void Intersect()
        {
            StandardBitBoards.MiddleCol.Intersect(StandardBitBoards.MiddleRow)
                .ShouldBe(StandardBitBoards.Centre);

            BitBoard.Full
                .Intersect(StandardBitBoards.Middle)
                .ShouldBe(StandardBitBoards.Middle);
        }

        [Fact]
        public void Union()
        {
            StandardBitBoards.MiddleCol.Union(StandardBitBoards.MiddleRow)
                .ShouldBe(StandardBitBoards.Middle);
        }

        [Fact]
        public void OutsideOf()
        {
            BitBoard.Full
                .NotIntersectingWith(StandardBitBoards.Middle)
                .ShouldBe(StandardBitBoards.Corners);
        }

        [Fact]
        public void GetHashCodeTests()
        {
            var b1 = new BitBoard(
                "111" +
                "000" +
                "111");

            b1.GetHashCode()
                .ShouldNotBe(StandardBitBoards.Top.GetHashCode());

            b1.GetHashCode()
                .ShouldNotBe(StandardBitBoards.Bottom.GetHashCode());

            b1.GetHashCode()
                .ShouldBe(
                    StandardBitBoards.Top.Union(
                        StandardBitBoards.Bottom)
                    .GetHashCode());
        }

        [Fact]
        public void InitTest()
        {
            StandardBitBoards.Bottom.ShouldNotBeNull();
            StandardBitBoards.Middle.ShouldNotBeNull();
            StandardBitBoards.Centre.ShouldNotBeNull();
            StandardBitBoards.Edge.ShouldNotBeNull();
            StandardBitBoards.Corners.ShouldNotBeNull();
        }
    }
}