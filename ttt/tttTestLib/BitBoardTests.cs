using Shouldly;

using System;
using System.Linq;

using tttLib;

using Xunit;

namespace tttTestLib
{
    public class BitBoardTests
    {
        [Fact]
        public void CtorTests()
        {
            Should.Throw<ArgumentException>(() =>
                new BitBoard("0000")); // !9 chars

            Should.Throw<ArgumentException>(() =>
                new BitBoard("0000000000")); // !9 chars

            Should.Throw<ArgumentException>(() =>
                new BitBoard("1111111111")); // !9 chars

            Should.Throw<ArgumentException>(() =>
                new BitBoard("000000002")); // 9 chars but not all [0|1]

            new BitBoard("000000000").ShouldNotBeNull();
            new BitBoard("111111111").ShouldNotBeNull();
            new BitBoard("1010-0101-1").ShouldNotBeNull();

            new BitBoard("    010\n    111\n    010 \n    ").ShouldNotBeNull();
        }

        [Fact]
        public void GetSetBitsCountTest()
        {
            new BitBoard("000000000").GetSetBitsCount().ShouldBe(0);
            new BitBoard("000000001").GetSetBitsCount().ShouldBe(1);
            new BitBoard("000000010").GetSetBitsCount().ShouldBe(1);
            new BitBoard("000000100").GetSetBitsCount().ShouldBe(1);
            new BitBoard("000001000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("000010000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("000100000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("001000000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("010000000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("100000000").GetSetBitsCount().ShouldBe(1);
            new BitBoard("100000001").GetSetBitsCount().ShouldBe(2);
            new BitBoard("100010001").GetSetBitsCount().ShouldBe(3);
            new BitBoard("101010101").GetSetBitsCount().ShouldBe(5);
            new BitBoard("010101010").GetSetBitsCount().ShouldBe(4);
            new BitBoard("010101011").GetSetBitsCount().ShouldBe(5);
            new BitBoard("111111111").GetSetBitsCount().ShouldBe(9);
        }

        [Fact]
        public void EqualsTest()
        {
            var bb1 = StandardBitBoards.MiddleCol
                .Union(StandardBitBoards.MiddleRow);

            var bb2 = new BitBoard("010" +
                "111" +
                "010");

            var isEqual = bb1.Equals((object)bb2);
            isEqual.ShouldBeTrue();
        }

        [Fact]
        public void InitTest()
        {
            StandardBitBoards.Bottom.ShouldNotBeNull();
            StandardBitBoards.Middle.ShouldNotBeNull();
            StandardBitBoards.Centre.ShouldNotBeNull();
            StandardBitBoards.Edge.ShouldNotBeNull();
            StandardBitBoards.Corners.ShouldNotBeNull();
        }

        [Fact]
        public void CellsTest()
        {
            var bb = new BitBoard("110 010 011");
            var cells = bb.ToCells().ToArray();
            cells.Length.ShouldBe(5);
            cells.ShouldContain((Cell)"C1");
            cells.ShouldContain((Cell)"B1");
            cells.ShouldContain((Cell)"B2");
            cells.ShouldContain((Cell)"A3");
            cells.ShouldContain((Cell)"B3");
            cells.ShouldNotContain((Cell)"C3");
            cells.ShouldNotContain((Cell)"A2");
            cells.ShouldNotContain((Cell)"C2");
            cells.ShouldNotContain((Cell)"A1");
        }

        [Fact]
        public void GetUnmarkedCellBitBoards_FullBoardTest()
        {
            var fullBoard = BitBoard.Full;
            var possibleMoveBitBoards = fullBoard.GetUnmarkedCellBitBoards();
            possibleMoveBitBoards.Any().ShouldBeFalse();
        }

        [Fact]
        public void GetUnmarkedCellBitBoards_EmptyCellsTest()
        {
            var fullBoard = new BitBoard("111011110");
            var possibleMoveBitBoards = fullBoard.GetUnmarkedCellBitBoards();
            possibleMoveBitBoards.Any().ShouldBeTrue();
            possibleMoveBitBoards.Count().ShouldBe(2);
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000000001"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000100000"));
        }

        [Fact]
        public void GetUnmarkedCellBitBoards_EmptyBoardTest()
        {
            var fullBoard = BitBoard.Empty;
            var possibleMoveBitBoards = fullBoard.GetUnmarkedCellBitBoards();
            possibleMoveBitBoards.Any().ShouldBeTrue();
            possibleMoveBitBoards.Count().ShouldBe(9);
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000000001"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000000010"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000000100"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000001000"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000010000"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("000100000"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("001000000"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("010000000"));
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("100000000"));
        }

        [Fact]
        public void GetUnmarkedCellBitBoards_WinningMoveTest()
        {
            var fullBoard = new BitBoard("000010100");
            var possibleMoveBitBoards = fullBoard.GetUnmarkedCellBitBoards();
            possibleMoveBitBoards
                .ShouldContain(new BitBoard("001000000"));
        }
    }
}