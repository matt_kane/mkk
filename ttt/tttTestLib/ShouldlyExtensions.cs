﻿using Shouldly;

using System.Linq;

using tttLib;

namespace tttTestLib
{
    internal static class ShouldlyExtensions
    {
        public static void MustBe(this BitBoard bb, string expected)
        {
            var str = bb.ToString()
                .Replace("\n", "");
            var trimmedExpected = new string(
                expected.Where(c => !char.IsWhiteSpace(c)).ToArray());

            str.ShouldBe(trimmedExpected);
        }
    }
}